from matplotlib import pyplot as plt
import cv2 as cv
import numpy as np
# %%
#TASK 1.1
sizeKernel = 3
kernel = np.ones((sizeKernel,sizeKernel), np.float32) / (sizeKernel*sizeKernel)
img = cv.imread("img/victoria.jpg")
img = img[:,:,::-1]
# %%
#TASK 1.1
blur = cv.filter2D(img, -1, kernel)
blur = cv.cvtColor(blur, cv.COLOR_BGR2RGB)
plt.imshow(blur[:,:,::-1])
plt.show()
# %%
#TASK 1.1
def myimplem(img, kernel):
    i_width, i_height = img.shape[1], img.shape[0] # Image calculation of width and height
    k_width, k_height = kernel.shape[1], kernel.shape[0] # Kernel calculation of width and height


    kh = np.floor_divide(k_height,2) # Calculation of kernel dividing by two to floor
    kw = np.floor_divide(k_width,2)
    img_con = np.zeros_like(img) # Creating new array that is filled with zeros of the image
    for i in range(kh,i_height-kh): # get image height pixel position
        for j in range(kw,i_width-kw): # get image width pixel position
            s = 0 # Sum of the values
            for m in range(k_height): # kernel position in array height/column
                for n in range(k_width): # kernel position in array width/row
                    s += np.multiply(kernel[m][n],img[i-kh+m][j-kw+n])
                img_con[i][j] = s
        plt.imshow(img_con)
        plt.show()
    return img_con
# %%
#TASK 1.1 and 1.2
img = cv.imread('img/victoria.jpg')[:,:,::-1]
result = myimplem(img, kernel)
# %%
#TASK 1.2
x = np.subtract(blur[:,:,1], result[:,:,1])
x
# %%
#TASK 2.2
# ORB
image1 = cv.imread("img/victoria.jpg", cv.IMREAD_GRAYSCALE)
image2 = cv.imread("img/victoria2.jpg", cv.IMREAD_GRAYSCALE)

def orb(image):
    # Initiate ORB detector
    orb = cv.ORB_create()
    # find the keypoints with ORB
    kp = orb.detect(image,None)
    # compute the descriptors with ORB
    kp, des = orb.compute(image, kp)
    # draw only keypoints location,not size and orientation
    img2 = cv.drawKeypoints(image, kp, None, color=(0,255,0), flags=0)
    plt.imshow(img2)
    plt.show()
    return None
# %%
orb(image1)
# %%
orb(image2)
# %%
#TASK 2.2
image1 = cv.imread("img/victoria.jpg", cv.IMREAD_GRAYSCALE)
image2 = cv.imread("img/victoria2.jpg", cv.IMREAD_GRAYSCALE)

def surf(image1):
    surf = cv.xfeatures2d.SURF_create(400)
    kp, des = surf.detectAndCompute(image1,None)
    surf.setHessianThreshold(4000)
    kp, des = surf.detectAndCompute(image1,None)
    img2 = cv.drawKeypoints(image1,kp,None,(255,0,0),4)
    plt.imshow(img2),plt.show()

# %%
surf(image1)
# %%
surf(image2)
# %%
#TASK 3
orb = cv.ORB_create()
kp1, des1 = orb.detectAndCompute(image1, None)
kp2, des2 = orb.detectAndCompute(image2, None)
bf = cv.BFMatcher(cv.NORM_HAMMING, crossCheck=True) # For better matching
matches = bf.match(des1, des2)
matches = sorted(matches, key = lambda x:x.distance)
matching_result = cv.drawMatches(image1, kp1, image2, kp2, matches, None)
plt.imshow(matching_result)
plt.show()
# %%
image1 = cv.imread("img/victoria.jpg", cv.IMREAD_GRAYSCALE)
image2 = cv.imread("img/victoria2.jpg", cv.IMREAD_GRAYSCALE)
sift = cv.xfeatures2d.SIFT_create()
kp1, des1 = sift.detectAndCompute(image1, None)

kp2, des2 = sift.detectAndCompute(image2, None)
bf = cv.BFMatcher() # For better matching
matches = bf.knnMatch(des1,des2,k=2)
good = []
for m,n in matches:
 if m.distance < 0.75*n.distance:
    good.append([m])
matching_result = cv.drawMatchesKnn(image1, kp1, image2, kp2, good, None)
plt.imshow(matching_result)
plt.show()
# %% [markdown]
#
# %%
image1 = cv.imread("img/victoria.jpg", cv.IMREAD_GRAYSCALE)
image2 = cv.imread("img/victoria2.jpg", cv.IMREAD_GRAYSCALE)
surf = cv.xfeatures2d.SURF_create(400)
kp1, des1 = surf.detectAndCompute(image1, None)
kp2, des2 = surf.detectAndCompute(image2, None)
bf = cv.BFMatcher() # For better matching
matches = bf.knnMatch(des1,des2,k=2)
good = []

for m,n in matches:
    if m.distance < 0.75*n.distance:
        good.append([m])

matching_result = cv.drawMatchesKnn(image1, kp1, image2, kp2, good, None)
plt.imshow(matching_result)
plt.show()